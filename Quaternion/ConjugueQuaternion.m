function [Q_conjugue] = ConjugueQuaternion(Q1)
Q_conjugue = [Q1(1) -Q1(2) -Q1(3) -Q1(4)]; 
end