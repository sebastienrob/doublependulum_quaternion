function [Q_multiplication] = MultiplicationQuaternion(Q1,Q2)
Q_multiplication = [Q1(1)*Q2(1)-Q1(2)*Q2(2)-Q1(3)*Q2(3)-Q1(4)*Q2(4) ; Q1(1)*Q2(2)+Q1(2)*Q2(1)+Q1(3)*Q2(4)-Q1(4)*Q2(3) ; Q1(1)*Q2(3)-Q1(2)*Q2(4)+Q1(3)*Q2(1)+Q1(4)*Q2(2) ; Q1(1)*Q2(4)+Q1(2)*Q2(3)-Q1(3)*Q2(2)+Q1(4)*Q2(1)];
Q_multiplication = Q_multiplication';
end