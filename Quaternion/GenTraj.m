function [CoeffTraj] = GenTraj(Tf,alpha_initial,alpha_final)
a = -2*(alpha_final - alpha_initial)/(Tf)^3;
b = 3 * (alpha_final - alpha_initial)/(Tf)^2;
c = 0;
d = alpha_initial;
CoeffTraj = [a b c d]; %1L et 7C
end