clear all
close all
clc

%% GENERAL PASSAGE ROTATION
R1_x = [1 ; 0 ; 0]; %pose vecteur R1x
R1_y = [0 ; 1 ; 0]; %pose vecteur R1y
R1_z = [0 ; 0 ; 1]; %pose vecteur R1z

alpha = pi/6;
n = [0 0 1]; %rotation direct d'angle alpha autour d'un axe (ICI Z) dirig� par le vecteur unitaire n
Qz = [cos(alpha/2) ; 0 ; 0 ; sin(alpha/2)]; %rotation autour de z 
Qx = [cos(alpha/2) ; sin(alpha/2) ; 0 ; 0]; %rotation autour de x
Qy = [cos(alpha/2) ; 0 ; sin(alpha/2) ; 0]; %rotation autour de y 
Q = Qz; %init
[C21_x,R2_x] = MatriceDePassage(Qz,R1_x);
[C21_y,R2_y] = MatriceDePassage(Qz,R1_y);
[C21_z,R2_z] = MatriceDePassage(Qz,R1_z);
R1_xx = [0 1 ; 0 0 ; 0 0]; %pose vecteur R1x
R1_yy = [0 0 ; 0 1 ; 0 0]; %pose vecteur R1y
R1_zz = [0 0 ; 0 0 ; 0 1]; %pose vecteur R1z
R2_xx = [0 R2_x(1) ; 0 R2_x(2) ; 0 R2_x(3)]; %pose vecteur R2x
R2_yy = [0 R2_y(1); 0 R2_y(2) ; 0 R2_y(3)]; %pose vecteur R2y
R2_zz = [0 R2_z(1) ; 0 R2_z(2) ; 0 R2_z(3)]; %pose vecteur R2z

%% AFFICHAGE INITIALISATION
fff = figure;
filename = 'quaternion1.gif'; %nom pour le film
hold on;
axis([-2 2 -2 2 -2 2]);
title('Visualisation rotation rep�re par rapport � un autre');
legend('axe x','axe y','axe z')
xlabel('axe x')
ylabel('axe y')
zlabel('axe z')
grid on;
view([90,90,90])
drawnow;
%tracer les axes du rep�re R1 de r�f�rence
repere1enx = line(R1_xx(1,:),R1_xx(2,:),R1_xx(3,:), 'LineWidth',2,'Color','blue'); %construction vecteur R1x
repere1eny = line(R1_yy(1,:),R1_yy(2,:),R1_yy(3,:), 'LineWidth',2,'Color','red'); %construction vecteur R1y
repere1enz = line(R1_zz(1,:),R1_zz(2,:),R1_zz(3,:), 'LineWidth',2,'Color','black'); %construction vecteur R1z
%tracer les axes du rep�re R2
hold on
repere2enx = line(R2_xx(1,:),R2_xx(2,:),R2_xx(3,:), 'LineWidth',2,'Color','green'); %construction vecteur R1x
repere2eny = line(R2_yy(1,:),R2_yy(2,:),R2_yy(3,:), 'LineWidth',2,'Color','yellow'); %construction vecteur R1y
repere2enz = line(R2_zz(1,:),R2_zz(2,:),R2_zz(3,:), 'LineWidth',2,'Color','black'); %construction vecteur R1z
alpha = 0;
dt = 1; %juste pour test
ite = 300;
%% ROTATION de R2 SELON Z puis X puis Y par rapport au rep�re de r�f�rence R1
for i = 1 : 1 : ite %i=t
    alpha = alpha+0.063;
    Q_precedent = Q;
    
if  i>0 && i<100
    Q = [cos(alpha/2) ; 0 ; 0 ; sin(alpha/2)]; %rotation autour de z
end
if  i>100 && i<200
    Q = [cos(alpha/2) ; sin(alpha/2) ; 0 ; 0]; %rotation autour de x
end
if  i>200 
    Q = [cos(alpha/2) ; 0 ; sin(alpha/2) ; 0]; %rotation autour de y
end

%% CALCUL DE DERIVE QUATERNION ET REPERE R2 /r a R1
Q_actuel = Q;
dotR2_x = VitesseParQuaternion(Q_precedent,Q_actuel,dt,R2_x); %vitesse point R2_x par rapport � xyz de 
dotR2_y = VitesseParQuaternion(Q_precedent,Q_actuel,dt,R2_y);
dotR2_z = VitesseParQuaternion(Q_precedent,Q_actuel,dt,R2_z);
dotR2_x_save(i,1:4) = dotR2_x;
dotR2_y_save(i,1:4) = dotR2_y;
dotR2_z_save(i,1:4) = dotR2_z;

%% METHODE 1
% [Q_conjugue] = ConjugueQuaternion(Qz);
% R1_x = [0 1 0 0];
% R1_y = [0 0 1 0];
% R1_z = [0 0 0 1];
% [R2_x_etape1] = MultiplicationQuaternion(Qz,R1_x);
% [R2_y_etape1] = MultiplicationQuaternion(Qz,R1_y);
% [R2_z_etape1] = MultiplicationQuaternion(Qz,R1_z);
% R2_xx = MultiplicationQuaternion(R2_x_etape1,Q_conjugue);
% R2_yy = MultiplicationQuaternion(R2_y_etape1,Q_conjugue);
% R2_zz = MultiplicationQuaternion(R2_z_etape1,Q_conjugue);

%% METHODE GENERAL
[C21_x,R2_x] = MatriceDePassage(Q,R1_x);
[C21_y,R2_y] = MatriceDePassage(Q,R1_y);
[C21_z,R2_z] = MatriceDePassage(Q,R1_z);

%% pose des 2 REPERES
R1_xx = [0 1 ; 0 0 ; 0 0]; %pose vecteur R1x
R1_yy = [0 0 ; 0 1 ; 0 0]; %pose vecteur R1y
R1_zz = [0 0 ; 0 0 ; 0 1]; %pose vecteur R1z
R2_xx = [0 R2_x(1) ; 0 R2_x(2) ; 0 R2_x(3)]; %pose vecteur R2x
R2_yy = [0 R2_y(1); 0 R2_y(2) ; 0 R2_y(3)]; %pose vecteur R2y
R2_zz = [0 R2_z(1) ; 0 R2_z(2) ; 0 R2_z(3)]; %pose vecteur R2z

%% ANIMATION 3D 
%tracer les axes du rep�re R1 de r�f�rence

set(repere1enx,'xdata',R1_xx(1,:),'ydata',R1_xx(2,:),'zdata',R1_xx(3,:));%construction vecteur R1x
set(repere1eny,'xdata',R1_yy(1,:),'ydata',R1_yy(2,:),'zdata',R1_yy(3,:));%construction vecteur R1y
set(repere1enz,'xdata',R1_zz(1,:),'ydata',R1_zz(2,:),'zdata',R1_zz(3,:));%construction vecteur R1z

set(repere2enx,'xdata',R2_xx(1,:),'ydata',R2_xx(2,:),'zdata',R2_xx(3,:));%construction vecteur R1x
set(repere2eny,'xdata',R2_yy(1,:),'ydata',R2_yy(2,:),'zdata',R2_yy(3,:));%construction vecteur R1y
set(repere2enz,'xdata',R2_zz(1,:),'ydata',R2_zz(2,:),'zdata',R2_zz(3,:));%construction vecteur R1z

 %% CREATION DU FILM POUR VISUALISATION    
  M(i) = getframe;
  im = frame2im(M(i)); 
  [imind,cm] = rgb2ind(im,256); 
  % �crire dans le fichier
  if i == 1 
     imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
  else 
     imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
  end

pause(0.01)
end

%% VISUALISATION VITESSES DU REPERE R2 PAR RAPPORT AU REPERE R1
dotR2_x_tot = dotR2_x_save(1:ite,2)+dotR2_x_save(1:ite,3)+dotR2_x_save(1:ite,4);
dotR2_y_tot = dotR2_y_save(1:ite,2)+dotR2_y_save(1:ite,3)+dotR2_y_save(1:ite,4);
dotR2_z_tot = dotR2_z_save(1:ite,2)+dotR2_z_save(1:ite,3)+dotR2_z_save(1:ite,4);

figure;
plot(1:ite,dotR2_x_tot);
hold on
plot(1:ite,dotR2_y_tot);
hold on
plot(1:ite,dotR2_z_tot);
title('Visualisation vitesse rotation rep�re par rapport � un autre');
legend('dotX2','dotY2','axe Z2')
xlabel('it�ration temporelle')
ylabel('vitesse')
grid on;
