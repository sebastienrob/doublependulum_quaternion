function [alpha_desire,derive_alpha_desire] = EvalTraj(CoeffTraj,t,dt)
alpha_desire = CoeffTraj(:,4) + CoeffTraj(:,3)*t + CoeffTraj(:,2)*t^2 + CoeffTraj(:,1)*t^3; %1L 7C
%%application num�rique ok jusqu'ici

if t==0
    derive_alpha_desire = 0;
else
    alpha_desire_precedent = CoeffTraj(:,4) + CoeffTraj(:,3)*(t-dt) + CoeffTraj(:,2)*(t-dt)^2 + CoeffTraj(:,1)*(t-dt)^3;
    derive_alpha_desire = (alpha_desire - alpha_desire_precedent)/(t-(t-dt));
end

end
