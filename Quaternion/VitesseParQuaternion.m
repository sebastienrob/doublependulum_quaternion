function [dotX2] = VitesseParQuaternion(Q_precedent,Q_actuel,dt,X2)
X2 =[0 X2(1) X2(2) X2(3)];
Q_conj = ConjugueQuaternion(Q_actuel);
dotQ = [Q_actuel(1)-Q_precedent(1)/dt Q_actuel(2)-Q_precedent(2)/dt Q_actuel(3)-Q_precedent(3)/dt Q_actuel(4)-Q_precedent(4)/dt];
T = MultiplicationQuaternion(dotQ,Q_conj);

etape1 = MultiplicationQuaternion(T,X2);
etape2 = MultiplicationQuaternion(X2,T);
dotX2 = etape1 - etape2;

end