function [K] = lineariser_Modele_DoublePendule_chariot(rob,etat_commande_stable) 

%% SIMPLIFICATION
h1 = rob.m + rob.m1 + rob.m2;
h2 = rob.m1 * rob.l1 + rob.m2 * rob.L1;
h3 = rob.m2 * rob.l2;
h4 = rob.m1 * rob.l1^2 + rob.m2 * rob.L1^2 + rob.J1;
h5 = rob.m2 * rob.l2 * rob.L1;
h6 = rob.m2 * rob.l2^2 + rob.J2;
h7 = rob.m1 * rob.l1 * rob.g + rob.m2 * rob.L1 * rob.g;
h8 = rob.m2 * rob.l2 * rob.g;

%% OPTENTION DU MODELE LINEAIRE
syms f x theta1 theta2 dx dtheta1 dtheta2 g Q dq q
M = [h1 h2 h3 ; h2 h4 h5 ; h3 h5 h6];
g = [0; -h7*theta1 ; -h8*theta2];
Q = [f ; 0 ; 0];
q = [x ; theta1 ; theta2];
dq = [dx ; dtheta1 ; dtheta2];
% ici j'ai M*ddq + g = f
%je cr�e mes 3 �quations �gal � 0
ddq = inv(M) * (Q - g);
ddq_equation_egal0(1) = [ddq(1) - dx];
ddq_equation_egal0(2) = [ddq(2) - dtheta1];
ddq_equation_egal0(3) = [ddq(3) - dtheta2];

%% CALCUL DES DERIVEE PARTIELLES
dpartielle_x_1 = diff(ddq_equation_egal0(1),dx);
dpartielle_f_1 = diff(ddq_equation_egal0(1),f);
dpartielle_theta1_1 = diff(ddq_equation_egal0(1),theta1);
dpartielle_theta2_1 = diff(ddq_equation_egal0(1),theta2);

dpartielle_dtheta1_2 = diff(ddq_equation_egal0(2),dtheta1);
dpartielle_f_2 = diff(ddq_equation_egal0(2),f);
dpartielle_theta1_2 = diff(ddq_equation_egal0(2),theta1);
dpartielle_theta2_2 = diff(ddq_equation_egal0(2),theta2);

dpartielle_dtheta2_3 = diff(ddq_equation_egal0(3),dtheta2);
dpartielle_f_3 = diff(ddq_equation_egal0(3),f);
dpartielle_theta1_3 = diff(ddq_equation_egal0(3),theta1);
dpartielle_theta2_3 = diff(ddq_equation_egal0(3),theta2);

%% CALCUL DE LA MATRICE A ET B
A = [0 0 0 1 0 0 ; 0 0 0 0 1 0 ; 0 0 0 0 0 1 ; 0 dpartielle_theta1_1 dpartielle_theta2_1 dpartielle_x_1 0 0 ; 0 dpartielle_theta1_2 dpartielle_theta2_2 0 dpartielle_dtheta1_2 0 ; 0 dpartielle_theta1_3 dpartielle_theta2_3 0 0 dpartielle_dtheta2_3]; 
B = [0 ; 0 ; 0 ; dpartielle_f_1 ; dpartielle_f_2 ; dpartielle_f_3];
%ici j'ai mon systeme DotDeltax = A*Deltax + B*Deltau

%% PLACEMENT DE POLE ET CALCUL DE LA MATRICE RETOUR D'ETAT
%placement de poles pour trouver R = [r1 r2 r3 r4 r5 r6]
%eig(A-B*R); CALCUL DES POLES
%syms r1 r2 r3 r4 r5 r6
%R = [r1 r2 r3 r4 r5 r6]
A=eval(A);
B=eval(B);
stabilite_sys = eig(A); 
p = [-2.1 -3.3 -2.5 -2.7 -3.9 -2.4];
K = place(A,B,p);

end






