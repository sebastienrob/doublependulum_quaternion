function etatdot = Modele_DoublePendule_chariot(t,etat,commandeQ,rob)

%% N.B.
%q = [x ; theta1 ; theta2]
%etat = [x ; theta1 ; theta2; xdot ; theta1dot; theta2dot]

%% Simplification
c1 = cos(etat(2));
s1 = sin(etat(2));
c2 = cos(etat(3));
s2 = sin(etat(3));
c1_moins2 = cos(etat(2) - etat(3));
s1_moins2 = sin(etat(2) - etat(3));
h1 = rob.m + rob.m1 + rob.m2;
h2 = rob.m1 * rob.l1 + rob.m2 * rob.L1;
h3 = rob.m2 * rob.l2;
h4 = rob.m1 * rob.l1^2 + rob.m2 * rob.L1^2 + rob.J1;
h5 = rob.m2 * rob.l2 * rob.L1;
h6 = rob.m2 * rob.l2^2 + rob.J2;
h7 = rob.m1 * rob.l1 * rob.g + rob.m2 * rob.L1 * rob.g;
h8 = rob.m2 * rob.l2 * rob.g;

%% MODELE DYNAMIQUE
M = [h1 h2*c1 h3*c2 ; h2*c1 h4 h5*c1_moins2 ; h3*c2 h5*c1_moins2 h6];
C = [0 -h2*etat(5)*s1 -h3*etat(6)*s2 ; 0 0 h5*etat(6)*s1_moins2 ; 0 -h5*etat(5)*s1_moins2 0];
g = [0 ; -h7*s1 ; -h8*s2];
Q = [commandeQ ; 0 ; 0];
new(1:3) = inv(M) * (Q - (C * [etat(4) ; etat(5) ; etat(6)]) - g);

%% MISE A JOUR
etatdot(1,1) = etat(4);
etatdot(2,1) = etat(5);
etatdot(3,1) = etat(6);
etatdot(4,1) = new(1);
etatdot(5,1) = new(2);
etatdot(6,1) = new(3);

end