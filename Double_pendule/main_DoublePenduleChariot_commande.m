%% INITIALISATION
clear all; 
close all;
  
%% INITIALISATION DU SYSTEME
theta1 = degtorad(1);
theta2 = degtorad(4);
x = 0;
xdot = 0;
theta1dot = 0;
theta2dot = 0;
etat = [x ; theta1 ; theta2 ; xdot ; theta1dot ; theta2dot];
etat_stable = [0 ; 0 ; 0 ; 0 ; 0 ; 0];
rob.g = 9.81;
rob.m = 150; %masse du chariot
rob.m1 = 10;  %masse du premier pendule
rob.m2 = 10; %masse du secons pendule
rob.l1 = 4; %distance pivot cdm
rob.l2 = 4; %distance pivot cdm
rob.L1 = 2*rob.l1; %longueur premier pendule
rob.L2 = 2*rob.l2; %longueur second pendule
rob.J1 = rob.m1*((rob.L1^2)/12);
rob.J2 = rob.m2*((rob.L2^2)/12);
T = 15; 
dt = 0.01;

%% DECLARATION DES TAILLES POUR AUGMENTER VITESSES CALCUL
taille_echantillon = T/dt;
temps = zeros(taille_echantillon,1);
etat_save = zeros(taille_echantillon,1);
pose_chariot_x = zeros(taille_echantillon,1);
pose_tige1_x = zeros(taille_echantillon,1);
pose_tige1_y = zeros(taille_echantillon,1);
pose_tige2_x = zeros(taille_echantillon,1);
pose_tige2_y = zeros(taille_echantillon,1);

%% AFFICHAGE INITIALISATION
fff = figure;
i=1; % pour le temps
filename = 'pendule_double.gif'; %nom pour le film (rapidite X3)
hold on;
axis([-10 10 -20 20]);
grid on;
xlabel('axe x')
ylabel('axe y')
title('Commande pendule double sans correcteur');
pose_chariot_x(1) = etat(1);
pose_tige1_x(1) = etat(1) + rob.L1*sin(etat(2));
pose_tige1_y(1) = rob.L1*cos(etat(2));
pose_tige2_x(1) = pose_tige1_x(1) + rob.L1*(sin(etat(3)));
pose_tige2_y(1) = pose_tige1_y(1) + rob.L1*(cos(etat(3)));
tige1 = line([x pose_tige1_x(1)],[0 pose_tige1_y(1)],'LineWidth',2,'Color','black'); %tige 1
tige2 = line([pose_tige1_x(1) pose_tige2_x(1)],[pose_tige1_y(1) pose_tige2_y(1)],'LineWidth',2,'Color','green'); %tige 2
chariot = plot(pose_chariot_x(1),0,'sb','markersize', 15); %chariot

%% CONTROLE DE LA COMMANDE X EN MODE MANUEL GRAPHIQUE
% ax = axes('Parent',fff,'position',[0.13 0.39  0.77 0.54]);
% h = stepplot(ax,sys);
% setoptions(h,'XLim',[0,10],'YLim',[0,2]);
% b = uicontrol('Parent',fff,'Style','slider','Position',[81,54,419,23],...
%               'value',x, 'min',0, 'max',1);
% bgcolor = fff.Color;
% bl1 = uicontrol('Parent',fff,'Style','text','Position',[50,54,23,23],...
%                 'String','0','BackgroundColor',bgcolor);

%% RETOUR D'ETAT ET PLACEMENT DE POLES + COMMANDE STABLE INIT
etat_commande_stable = [0 ; 0 ; 0 ; 0 ; 0 ; 0];
[R] = lineariser_Modele_DoublePendule_chariot(rob,etat_commande_stable); 


%%%%%%%%%%%%%%%%%%%GO PROCESS%%%%%%%%%%%%%%%%%%%%%%%%%


%% BOUCLE ASSERVISSEMENT CONTR�LE
for t = 0 : dt : T
    
  %% MISE A JOUR AFFICHAGE
  set(tige1, 'xdata', [pose_chariot_x(i,1) pose_tige1_x(i,1)], 'ydata', [0 pose_tige1_y(i,1)]);
  set(tige2, 'xdata', [pose_tige1_x(i,1) pose_tige2_x(i,1)], 'ydata', [pose_tige1_y(i,1) pose_tige2_y(i,1)]);
  set(chariot,'xdata', pose_chariot_x(i,1));
  drawnow;
    
  %% COMMANDE
  if i == 1
  commandeQ = etat(1);
  end
  
  %condition initial = etat // integre entre 0 et dt // int�gre etatdot de
  %mon modele // je lui donne commande,rob,etat pour le Calcul du md et ensuite j'int�gre au temps t 
  %% ODE45 ET SAUVEGARDE
  [t45, x45] = ode45(@(t,etat) Modele_DoublePendule_Chariot(t, etat, commandeQ, rob), [0 dt], etat);
  l45 = length(t45); %nombre de temps d'int�gration entre 0 et dt
  etat = x45(l45,:)'; %on prend la derni�re valeur d'int�gration
  etat_save(i,1) = etat(1); %x
  etat_save(i,2) = etat(2); %theta1
  etat_save(i,3) = etat(3); %theta2
  etat_save(i,4) = etat(4); %xdot
  etat_save(i,5) = etat(5); %theta1dot
  etat_save(i,6) = etat(6); %theta2dot
  
  pose_chariot_x(i+1,1) = etat_save(i,1);
  pose_tige1_x(i+1,1) = etat(1) + rob.L1*sin(etat_save(i,2));
  pose_tige1_y(i+1,1) =  rob.L1*cos(etat_save(i,2));
  pose_tige2_x(i+1,1) = pose_tige1_x(i+1,1) + rob.L1*sin(etat_save(i,3));
  pose_tige2_y(i+1,1) = pose_tige1_y(i+1,1) + rob.L1*cos(etat_save(i,3));
  
  %% MISE A JOUR DE LA COMMANDE
  etat_commande_MD = [etat(1) ; etat(2) ; etat(3) ; etat(4) ; etat(5) ; etat(6)];
  delta_x = etat_commande_MD - etat_commande_stable;
  delta_f = -R * delta_x;
  commandeQ = delta_f;
  
  %% CREATION DU FILM POUR VISUALISATION    
  M(i) = getframe;
  im = frame2im(M(i)); 
  [imind,cm] = rgb2ind(im,256); 
  % �crire dans le fichier
  if i == 1 
     imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',dt*3); 
  else 
     imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',dt*3); 
  end
     
  %% INCREMENTATION TEMPORELLE
  temps(i,1) = t;
  i = i + 1; %compteur time
end

%% AFFICHAGE ETAT
%x
figure;
plot(temps,etat_save(:,1))
xlabel('temps en s')
grid on
ylabel('distance selon x en m')
title('�volution de la position du chariot sur axe x en fonction du temps')

%theta1 et theta2
figure;
plot(temps,etat_save(:,2))
hold on
plot(temps,etat_save(:,3))
xlabel('temps en s')
grid on
ylabel('angle en rad/s')
title('�volution des angles theta1/theta2 en fonction du temps')
legend('tehta1','theta2')

%dtheta1 et dtheta2
figure;
plot(temps,etat_save(:,5))
hold on
plot(temps,etat_save(:,6))
xlabel('temps en s')
grid on
ylabel('angle en rad/s')
title('�volution des angles dtheta1/dtheta2 en fonction du temps')
legend('dtehta1','dtheta2')